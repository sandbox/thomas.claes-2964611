<?php

/**
 * Implements hook_discount_commerce_discount_rule_build().
 */
function domain_commerce_discount_commerce_discount_rule_build(&$rule, $discount) {

  // Always add a domain access check as an condition when creating a discount.
  $rule->condition('domain_commerce_discount_domain_compatibility_check', [
    'commerce_discount' => $discount->name,
  ]);
}

/**
 * Implements hook_rules_condition_info().
 */
function domain_commerce_discount_rules_condition_info() {
  $conditions = [];

  $conditions['domain_commerce_discount_domain_compatibility_check'] = [
    'label' => t('Check discount and user domain compatibility'),
    'parameter' => [
      'commerce_discount' => [
        'type' => 'token',
        'label' => t('Discount'),
        'description' => t('A discount with a compatibility strategy that requires evaluation.'),
        'options list' => 'commerce_discount_entity_list',
      ],
    ],
    'group' => t('Commerce Discount'),
  ];

  return $conditions;
}

/**
 * Rules callback of "Check discount and user domain compatibility" condition.
 *
 * Check whether the requesting user is on the same domain as the discount.
 *
 * @param string $discount_name
 *   The discount name whose compatibility needs to be checked.
 *
 * @return bool
 *   Returns TRUE if nothing has disqualified compatibility.
 */
function domain_commerce_discount_domain_compatibility_check($discount_name) {
  global $user;

  // Ensure the discount we're loading still exists.
  if (!$discount = entity_load_single('commerce_discount', $discount_name)) {
    return FALSE;
  }

  if (empty($discount->domain_id)) {
    return FALSE;
  }

  $full_user = NULL;
  if (!empty($user->uid)) {
    $full_user = user_load($user->uid);
  }

  // If the user is assigned to the same domain, the test will pass.
  if (!empty($full_user->domain_user)) {
    foreach ($full_user->domain_user as $did => $string_did) {
      if ((int) $discount->domain_id === (int) $did) {
        return TRUE;
      }
    }
  }

  return FALSE;
}
